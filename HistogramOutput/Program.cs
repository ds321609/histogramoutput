﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HistogramOutput
{
    class Program
    {
        static void Main(string[] args)
        {
            var inputs = new List<int> { 4, 1, 4, 2, 2, 3, 2, 4, 4, 5, 5, 9, 9, 9 };
            LogOutput.OutputHistogram(inputs);
        }
    }
}
