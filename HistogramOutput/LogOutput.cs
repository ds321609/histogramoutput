﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HistogramOutput
{
    public static class LogOutput
    {
        public static void OutputHistogram(List<int> inputs)
        {
            Dictionary<int, int> histogram = new Dictionary<int, int>();
            int currentRow = 1;

            foreach (var item in inputs)
            {
                if (histogram.ContainsKey(item))
                {
                    histogram[item] += 1;
                }
                else
                {
                    histogram.Add(item, 1);
                }
            }

            currentRow = histogram.Values.ToList().OrderByDescending(b => b).FirstOrDefault();

            while (currentRow > 0)
            {
                foreach (var kvp in histogram)
                {
                    if (kvp.Value >= currentRow)
                    {
                        Console.Write("*");
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                }

                Console.Write(Environment.NewLine);
                currentRow--;
            }

            string histogramKeys = string.Join('\0', histogram.Keys.ToArray());

            Console.WriteLine(histogramKeys);
        }
    }
}
